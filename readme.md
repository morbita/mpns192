#mini_project-mpns192- client and server
## Name: Mor Bitan

## Instructions:
* *Server*:
    1. Git clone https://bitbucket.org/morbita/mpns192/src/master/
    2. Add as maven project
    3. Set up sdk in the project
    4. Open tomcat7 in maven projects window
    5. Run tomcat7:run
    6. Open the link to a client that is being launched from the server:
        http://localhost:8080/mini_project/mini_project/

    **important- After doing all the instructions for the server, you have a client and a server that runs
           together. If you want to lunch a client that separated from the server
            (and see the client code) follow the next instructions.


* *client*:
    1. Git clone https://bitbucket.org/morbita/mpns192_client/src/master/
    2. Open the project
    3. In the terminal of the project write: npm install
    4. In the terminal of the project write: ng serve
    5. Open the link to a client separated from the server: http://localhost:4200/

## Description & More:
* Find in the pdf file: mini-project192.pdf

