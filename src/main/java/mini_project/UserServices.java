package mini_project;
import com.google.gson.Gson;
import mini_project.MusicFiles.MusicFile;
import mini_project.MusicFiles.MyMusicFiles;
import mini_project.enums.Category;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;



/**
 * This class contains all the HTTP requests for users.
 */

@Path("/file")
public class UserServices {
    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(
            @FormDataParam("file") InputStream inputStream,
            @FormDataParam("file")FormDataContentDisposition fileDetails,
            @FormDataParam("length") double length,
            @FormDataParam("category") int category)  {

        String currentDir = System.getProperty("user.dir");
        //writing the file to suspected directory that the user doesn't have permission to- we don't know if it's a virus yet
        String location = currentDir + "\\suspected\\" + fileDetails.getFileName();
        MyMusicFiles.getInstance().writeFileToLocation(location,inputStream);
        //check that the file is not a virus
        if(!(MyMusicFiles.getInstance().ScanFile(location,fileDetails.getFileName(),length, category))) {
            MyMusicFiles.getInstance().addNewSong();
            location = currentDir + "\\files\\" + fileDetails.getFileName();
            MyMusicFiles.getInstance().writeFileToLocation(location,inputStream);;
            return Response.status(Response.Status.OK).entity(new Gson().toJson("all good")).header("Access-Control-Allow-Origin", "*").build();
        }
        return Response.status(Response.Status.FORBIDDEN).entity("ERROR: This is a Virus").header("Access-Control-Allow-Origin", "*").build();
    }



    @POST
    @Path("/download")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces("text/plain")
    public Response downloadFile(  @FormDataParam("fileName") String fileName) {
        String currentDir = System.getProperty("user.dir");
        String location = currentDir + "\\files\\" + fileName;
        File file = new File(location);
        Response.ResponseBuilder responseBuilder = Response.ok(file);
        responseBuilder.header("Content-Disposition","attachment; filename="+fileName);
        return responseBuilder.build();
    }

    @GET
    @Path("/getSongs")
    public Response getSongs() {
        Gson gson = new Gson();
        MusicFile[] musicFiles= MyMusicFiles.getInstance().getAllFiles();
        String files = gson.toJson(musicFiles);
        return Response.status(Response.Status.OK).entity(files).header("Access-Control-Allow-Origin", "*").build();
    }

}



