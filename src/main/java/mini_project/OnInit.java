package mini_project;

import mini_project.MusicFiles.File;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mini_project.MusicFiles.MyMusicFiles;

import javax.servlet.http.HttpServlet;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Timer;
import java.util.concurrent.TimeUnit;


/**
 * This class is called when the server starts, used for update our queue.
 */

public class OnInit extends HttpServlet {
    public void init() {
        //initialize MusicFiles
        MyMusicFiles.getInstance();
    }
}