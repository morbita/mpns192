package mini_project.MusicFiles;

import mini_project.enums.Category;
/**
 * I choose that my server will be a music sever that has a music files
 */

public class MusicFile extends File{
    private double fileDuration;
    private Category myCategory;

    public MusicFile(String id, String name, double duration, Category category){
        super(id,name);
        this.fileDuration=duration;
        this.myCategory=category;
    }

    public double getDuration() {
        return fileDuration;
    }
    public Category getCategory() {
        return myCategory;
    }


}
