package mini_project.MusicFiles;

import com.kanishka.virustotal.dto.FileScanReport;
import com.kanishka.virustotal.dto.ScanInfo;
import com.kanishka.virustotal.dto.VirusScanInfo;
import com.kanishka.virustotal.exception.APIKeyNotFoundException;
import com.kanishka.virustotal.exception.UnauthorizedAccessException;
import com.kanishka.virustotalv2.VirusTotalConfig;
import com.kanishka.virustotalv2.VirustotalPublicV2;
import com.kanishka.virustotalv2.VirustotalPublicV2Impl;
import mini_project.enums.Category;
import org.jboss.resteasy.util.Hex;
import java.io.*;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;


//***
public class MyMusicFiles {
   private static  MyMusicFiles _instance=null;
   private ArrayList<MusicFile> serverFiles=null;
   private ArrayList<MusicFile> tmpFiles=new ArrayList<>();
   private Connection conn = null;
   private final String  API_KEY="2f1108f0f418640d138d1b3cc52b0cae823150f6f4066d91177f4499cc419ea8";

    //Constructor
    private MyMusicFiles(){
       serverFiles=new ArrayList<>();
       readFromDB();
   }

    //Singleton
    public static MyMusicFiles getInstance(){
        //initialize only once
        if (_instance == null)
            _instance = new MyMusicFiles();
        return _instance;
    }

    //When we init the sever we need to read all the files that exist from DB
    private void readFromDB() {
        setConnection();
        String query = "SELECT * FROM tblMusicFiles";
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                if(rs.getString("id")!=null)
                addFileToArray(rs.getString("id"), rs.getString("name"), rs.getDouble("duration"),
                        Category.fromInt(rs.getInt("category")));
            }
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // init our array of musicFiles from the DB
    private void addFileToArray(String fileId, String filename, double fileDuration, Category fileCatrgory){
        serverFiles.add(new MusicFile(fileId,filename,fileDuration,fileCatrgory));
    }

    //add new file that a user is uploading
    private void addFileToArray(){
        MusicFile musicFile=tmpFiles.get(tmpFiles.size()-1);
        if(musicFile.getIsChecked()) {
            serverFiles.add(musicFile);
        }
        deleteFromTmp();
    }

    // set a connection to our DB
    private void setConnection() {
        try {
            Class.forName("org.sqlite.JDBC");
            String currentDir = System.getProperty("user.dir");
            String location = currentDir + "\\src\\main\\miniProject.db";
            // db parameters
            String url = "jdbc:sqlite:"+location;
            // create a connection to the database
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    // close the connection to the DB
    private void closeConnection(){
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) { /* ignored */}
        }
    }

    // add a new song to the array and the DB
   public void addNewSong(){
       addFileToArray();
       addToDB();
    }

    private void addToDB() {
        Statement statement = null;
        MusicFile musicFile=serverFiles.get(serverFiles.size()-1);
        String id= musicFile.getId();
        String name=musicFile.getName();
        double duration=musicFile.getDuration();
        int category= musicFile.getCategory().ordinal();
        try {
            statement = conn.createStatement();
            String query="INSERT INTO tblMusicFiles " + "VALUES (?, ?, ?, ?)";
            statement.executeUpdate(query);
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, id);
            pstmt.setString(2, name);
            pstmt.setDouble(3, duration);
            pstmt.setInt(4, category);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder=new StringBuilder();
        for (MusicFile serverFile : serverFiles) {
            if (serverFile != null) {
                stringBuilder.append(serverFile.getName()).append(", ");
            }
        }
        return stringBuilder.toString();
    }


   public boolean ScanFile(String location,String name, double duration, int category) {
        try {

            VirusTotalConfig.getConfigInstance().setVirusTotalAPIKey(API_KEY);
            VirustotalPublicV2 virusTotalRef = new VirustotalPublicV2Impl();

            ScanInfo scanInformation = virusTotalRef.scanFile(new File(location));

            System.out.println("___SCAN INFORMATION FROM VIRUS TOTAL___");
            String id = Hex.encodeHex(scanInformation.getMd5().getBytes());
            addNewTmpFile(id,name,duration,Category.fromInt(category));
            return getFileScanReport(scanInformation.getResource());
        } catch (APIKeyNotFoundException ex) {
            System.err.println("API Key not found! " + ex.getMessage());
        } catch (UnsupportedEncodingException ex) {
            System.err.println("Unsupported Encoding Format!" + ex.getMessage());
        } catch (UnauthorizedAccessException ex) {
            System.err.println("Invalid API Key " + ex.getMessage());
        } catch (Exception ex) {
            System.err.println("Something Bad Happened! " + ex.getMessage());
        }
        return false;

    }

    private void addNewTmpFile(String id, String name, double duration, Category category) {
       MusicFile musicFile=new MusicFile(id,name,duration,category);
       tmpFiles.add(musicFile);
    }

    private boolean getFileScanReport(String resource) {
       try {
            VirusTotalConfig.getConfigInstance().setVirusTotalAPIKey(API_KEY);
            VirustotalPublicV2 virusTotalRef = new VirustotalPublicV2Impl();
           System.out.println("___GET REPORT FROM VIRUS TOTAL___");
           FileScanReport report = virusTotalRef.getScanReport(resource);
           Map<String, VirusScanInfo> scans=(report.getScans());
            while (scans==null) {
                //busy wait
                // max for 4 requests per minute
                Thread.sleep(25011);
                report = virusTotalRef.getScanReport(resource);
                scans = report.getScans();
           }
           System.out.println("___CHECK THAT THE FILE IS NOT A VIRUS___");
            for (String key : scans.keySet()) {
                VirusScanInfo virusInfo = scans.get(key);
                if(virusInfo.isDetected()){
                    deleteFromTmp();
                    return true;
                }
            }
            theFileWasChecked();

        } catch (APIKeyNotFoundException ex) {
            System.err.println("API Key not found! " + ex.getMessage());
        } catch (UnsupportedEncodingException ex) {
            System.err.println("Unsupported Encoding Format!" + ex.getMessage());
        } catch (UnauthorizedAccessException ex) {
            System.err.println("Invalid API Key " + ex.getMessage());
        } catch (Exception ex) {
            System.err.println("Something Bad Happened! " + ex.getMessage());
        }
        return false;
    }

    private void theFileWasChecked() {
       tmpFiles.get(tmpFiles.size()-1).setIsChecked();
    }

    private void deleteFromTmp() {
       tmpFiles.remove(tmpFiles.size()-1);
    }

    public void writeFileToLocation(String location, InputStream inputStream) {

        File file = new File(location);
        try {
            OutputStream out = new FileOutputStream(file);
            int i = 0;
            byte[] bytes = new byte[1024];

            while ((i = inputStream.read(bytes)) != -1) {
                out.write(bytes, 0, i);
            }
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public MusicFile[] getAllFiles() {
        MusicFile[] filesToSend = new MusicFile[serverFiles.size()];
        if (serverFiles.size() > 0) {
            serverFiles.toArray(filesToSend);
        }
        return filesToSend;
    }
}
