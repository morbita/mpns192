package mini_project.MusicFiles;

import mini_project.enums.Category;

/**
 * This class is an abstract class for the files in the server
 */

public abstract class File {
    private String name;
    private String id;
    private boolean isChecked;

    // constructor- isChecked initialize as false until vitus total will approve that the file is not a virus
    public File(String fileId,String fileName){
        this.id=fileId;
        this.name=fileName;
        this.isChecked = false;
    }

    public String getName() {
        return name;
    }
    public String getId() {return id;}
    public boolean getIsChecked(){return isChecked;}
    public void setIsChecked(){
        this.isChecked=true;
    }

    @Override
    public boolean equals(Object obj) {
       return name.equals(((File)obj).getName());
    }
}
