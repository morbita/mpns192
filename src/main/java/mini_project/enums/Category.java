package mini_project.enums;
/**
 * This class is enum that represent the job status.
 */
public enum Category {
    LOVE,
    ROCK,
    HIP_HOP,
    OPERA,
    JAZZ,
    LATIN,
    FUNK;

    public static Category fromInt(int category) {
        switch (category) {
            case 0:
                return LOVE;
            case 1:
                return ROCK;
            case 2:
                return HIP_HOP;
            case 3:
                return OPERA;
            case 4:
                return JAZZ;
            case 5:
                return LATIN;
            case 6:
                return FUNK;
        }
        return null;
    }

    public static int fromString(String category) {
        switch (category) {
            case "LOVE":
                return 0;
            case "ROCK":
                return 1;
            case "HIP_HOP":
                return 2;
            case "OPERA":
                return 3;
            case "JAZZ":
                return 4;
            case "LATIN":
                return 5;
            case "FUNK":
                return 6;
        }
        return -1;
    }
}